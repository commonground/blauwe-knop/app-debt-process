// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package appdebtprocess_test

import (
	"errors"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"appdebtprocess"
	"appdebtprocess/mock"
)

const MockLinkToken = "mock-link-token"

func TestSchuldenUseCase_schuldenOverzichtByBSN(t *testing.T) {
	type fields struct {
		debtRegisterRepository appdebtprocess.DebtRegisterRepositoryEndpoint
		linkRegisterRepository appdebtprocess.LinkRegisterRepository
	}
	type args struct {
		token string
	}
	tests := []struct {
		name          string
		fields        fields
		args          args
		want          *appdebtprocess.SchuldenOverzicht
		expectedError error
	}{
		{
			name: "without link token",
			fields: fields{
				debtRegisterRepository: func() appdebtprocess.DebtRegisterRepositoryEndpoint {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockDebtRegisterRepositoryEndpoint(ctrl)
					return repo
				}(),
				linkRegisterRepository: func() appdebtprocess.LinkRegisterRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockLinkRegisterRepository(ctrl)
					return repo
				}(),
			},
			args: args{
				"",
			},
			want:          nil,
			expectedError: appdebtprocess.ErrEmptyLinkToken,
		},
		{
			name: "link token does not exist",
			fields: fields{
				debtRegisterRepository: func() appdebtprocess.DebtRegisterRepositoryEndpoint {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockDebtRegisterRepositoryEndpoint(ctrl)
					repo.EXPECT().GetSchulden(gomock.Any()).Return(nil, appdebtprocess.ErrUnknownOverzicht).AnyTimes()
					return repo
				}(),
				linkRegisterRepository: func() appdebtprocess.LinkRegisterRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockLinkRegisterRepository(ctrl)
					repo.EXPECT().GetBSNFromLinkToken(gomock.Any()).Return(nil, errors.New("error ")).AnyTimes()
					return repo
				}(),
			},
			args: args{
				MockLinkToken,
			},
			want:          nil,
			expectedError: appdebtprocess.ErrLinkDoesNotExist,
		},
		{
			name: "find overzicht fails ",
			fields: fields{
				debtRegisterRepository: func() appdebtprocess.DebtRegisterRepositoryEndpoint {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					bsnModel, _ := appdebtprocess.NewBSN("814859094")

					repo := mock.NewMockDebtRegisterRepositoryEndpoint(ctrl)
					repo.EXPECT().GetSchulden(*bsnModel).Return(nil, errors.New("error")).AnyTimes()
					return repo
				}(),
				linkRegisterRepository: func() appdebtprocess.LinkRegisterRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					bsnModel, _ := appdebtprocess.NewBSN("814859094")

					repo := mock.NewMockLinkRegisterRepository(ctrl)
					repo.EXPECT().GetBSNFromLinkToken("mock-token").Return(bsnModel, nil).AnyTimes()

					return repo
				}(),
			},
			args: args{
				"mock-token",
			},
			want:          nil,
			expectedError: errors.New("error"),
		},
		{
			name: "with valid link token ",
			fields: fields{
				debtRegisterRepository: func() appdebtprocess.DebtRegisterRepositoryEndpoint {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					bsnModel, _ := appdebtprocess.NewBSN("814859094")

					schuld := appdebtprocess.NewSchuld(
						"Onroerende zaakbelasting",
						100,
						time.Date(2019, 06, 05, 0, 0, 0, 0, time.UTC),
						false,
					)

					repo := mock.NewMockDebtRegisterRepositoryEndpoint(ctrl)
					repo.EXPECT().GetSchulden(*bsnModel).Return(&[]appdebtprocess.Schuld{*schuld}, nil).AnyTimes()
					return repo
				}(),
				linkRegisterRepository: func() appdebtprocess.LinkRegisterRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					bsnModel, _ := appdebtprocess.NewBSN("814859094")

					repo := mock.NewMockLinkRegisterRepository(ctrl)
					repo.EXPECT().GetBSNFromLinkToken("mock-token").Return(bsnModel, nil).AnyTimes()

					return repo
				}(),
			},
			args: args{
				"mock-token",
			},
			want: func() *appdebtprocess.SchuldenOverzicht {
				schuld := appdebtprocess.NewSchuld(
					"Onroerende zaakbelasting",
					100,
					time.Date(2019, 06, 05, 0, 0, 0, 0, time.UTC),
					false,
				)
				appdebtprocessOverzicht := appdebtprocess.NewSchuldenOverzicht("my-organization", []appdebtprocess.Schuld{*schuld})
				return appdebtprocessOverzicht
			}(),
			expectedError: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := appdebtprocess.NewSchuldenUseCase(tt.fields.debtRegisterRepository, tt.fields.linkRegisterRepository, "my-organization")
			got, err := d.OverzichtByToken(tt.args.token)
			assert.Equal(t, tt.expectedError, err)

			assert.Equal(t, tt.want, got)

		})
	}
}
