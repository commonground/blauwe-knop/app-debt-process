// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package appdebtprocess

import (
	"sort"
	"time"
)

type SchuldenOverzicht struct {
	organisatie string
	schulden    []Schuld
}

func NewSchuldenOverzicht(organisatie string, schulden []Schuld) *SchuldenOverzicht {
	d := &SchuldenOverzicht{
		organisatie: organisatie,
		schulden:    schulden,
	}

	return d
}

func (d *SchuldenOverzicht) Organisatie() string {
	return d.organisatie
}

func (d *SchuldenOverzicht) TotaalSaldo() int {
	var result int

	for _, v := range d.schulden {
		result += v.saldo
	}

	return result
}

func (d *SchuldenOverzicht) SaldoDatumLaatsteSchuld() *time.Time {
	sort.Slice(d.schulden, func(i, j int) bool {
		return d.schulden[i].saldoDatum.After(d.schulden[j].saldoDatum)
	})
	if len(d.schulden) > 0 {
		return &d.schulden[0].saldoDatum
	}
	return nil
}

func (d *SchuldenOverzicht) Schulden() []Schuld {
	return d.schulden
}
