// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package appdebtprocess

import (
	"errors"

	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"
)

type DebtRegisterRepositoryEndpoint interface {
	GetSchulden(bsn BSN) (*[]Schuld, error)
	healthcheck.Checker
}

type LinkRegisterRepository interface {
	GetBSNFromLinkToken(linkToken string) (*BSN, error)
	healthcheck.Checker
}

// ErrUnknownOverzicht is used when no appdebtprocess overzicht could be found.
var ErrUnknownOverzicht = errors.New("no appdebtprocess overzicht available")
var ErrEmptyLinkToken = errors.New("invalid link token")
var ErrLinkDoesNotExist = errors.New("link does not exist")

type SchuldenUseCase struct {
	debtRegisterRepository DebtRegisterRepositoryEndpoint
	linkRegisterRepository LinkRegisterRepository
	organization           string
}

func NewSchuldenUseCase(debtRegisterRepository DebtRegisterRepositoryEndpoint, linkRegisterRepository LinkRegisterRepository, organization string) *SchuldenUseCase {
	return &SchuldenUseCase{
		debtRegisterRepository,
		linkRegisterRepository,
		organization,
	}
}

func (d *SchuldenUseCase) OverzichtByToken(token string) (*SchuldenOverzicht, error) {
	if token == "" {
		return nil, ErrEmptyLinkToken
	}

	bsn, err := d.linkRegisterRepository.GetBSNFromLinkToken(token)
	if err != nil {
		return nil, ErrLinkDoesNotExist
	}

	schulden, err := d.debtRegisterRepository.GetSchulden(*bsn)
	if err != nil {
		return nil, err
	}

	result := NewSchuldenOverzicht(d.organization, *schulden)

	return result, nil
}

func (a *SchuldenUseCase) GetHealthChecks() []healthcheck.Checker {
	return []healthcheck.Checker{
		a.debtRegisterRepository,
		a.linkRegisterRepository,
	}
}
