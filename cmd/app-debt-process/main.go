// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package main

import (
	"fmt"
	"log"
	"net/http"

	"go.uber.org/zap"

	"appdebtprocess"
	http_infra "appdebtprocess/http"
)

type options struct {
	ListenAddress       string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:8080" description:"Address for the app debt process to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	Organization        string `long:"organization" env:"ORGANIZATION" description:"Organization name"`
	LinkRegisterAddress string `long:"link-register-address" env:"LINK_REGISTER_ADDRESS" default:"http://localhost:8085" description:"Link register address."`
	LinkRegisterAPIKey  string `long:"link-register-api-key" env:"LINK_REGISTER_API_KEY" default:"" description:"API key to use when calling the link-register service."`
	DebtRegisterAddress string `long:"debt-register-address" env:"DEBT_REGISTER_ADDRESS" default:"http://localhost:8088" description:"Debt register address."`
	DebtRegisterAPIKey  string `long:"debt-register-api-key" env:"DEBT_REGISTER_API_KEY" default:"" description:"API key to use when calling the debt-register service."`

	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	if len(cliOptions.Organization) < 1 {
		log.Fatalf("please specify an organization")
	}

	logger, err := newZapLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	linkRegisterRepository := http_infra.NewLinkRegisterRepository(cliOptions.LinkRegisterAddress, cliOptions.LinkRegisterAPIKey)
	overzichtRepository := http_infra.NewDebtRegisterRepository(cliOptions.DebtRegisterAddress, cliOptions.DebtRegisterAPIKey)
	schuldenUseCase := appdebtprocess.NewSchuldenUseCase(overzichtRepository, linkRegisterRepository, cliOptions.Organization)
	router := http_infra.NewRouter(schuldenUseCase)

	logger.Info(fmt.Sprintf("start listening on %s", cliOptions.ListenAddress))
	err = http.ListenAndServe(cliOptions.ListenAddress, router)
	if err != nil {
		if err != http.ErrServerClosed {
			panic(err)
		}
	}
}

func newZapLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
