// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package appdebtprocess

import "time"

type Schuld struct {
	typeSchuld            string
	saldo                 int
	saldoDatum            time.Time
	vorderingOvergedragen bool
}

func NewSchuld(typeSchuld string, saldo int, saldoDatum time.Time, vorderingOvergedragen bool) *Schuld {
	return &Schuld{
		typeSchuld:            typeSchuld,
		saldo:                 saldo,
		saldoDatum:            saldoDatum,
		vorderingOvergedragen: vorderingOvergedragen,
	}
}

func (d Schuld) TypeSchuld() string {
	return d.typeSchuld
}

func (d Schuld) Saldo() int {
	return d.saldo
}

func (d Schuld) SaldoDatum() time.Time {
	return d.saldoDatum
}

func (d Schuld) VorderingOvergedragen() bool {
	return d.vorderingOvergedragen
}
