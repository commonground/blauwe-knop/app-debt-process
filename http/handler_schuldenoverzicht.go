// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"log"
	"net/http"

	"github.com/go-chi/render"

	"appdebtprocess"
)

func handlerOverzicht(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()
	schuldenUseCase, _ := ctx.Value(schuldenUseCaseKey).(*appdebtprocess.SchuldenUseCase)

	linkToken := req.Header.Get("Authorization")

	schuldenOverzicht, err := schuldenUseCase.OverzichtByToken(linkToken)
	if err == appdebtprocess.ErrEmptyLinkToken {
		log.Printf("link token required")
		http.Error(w, "link token required", http.StatusBadRequest)
		return
	} else if err == appdebtprocess.ErrLinkDoesNotExist {
		log.Printf("invalid link token")
		http.Error(w, "invalid link token", http.StatusUnauthorized)
		return
	} else if err != nil {
		log.Printf("unable to get appdebtprocess overzicht for bsn: %v", err)
		http.Error(w, "unable to get appdebtprocessoverzicht", http.StatusInternalServerError)
		return
	}

	viewModel := generateOverzichtViewModel(*schuldenOverzicht)
	render.JSON(w, req, &viewModel)
}
