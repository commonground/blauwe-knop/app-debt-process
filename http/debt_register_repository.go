// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"appdebtprocess"

	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"
)

type DebtRegisterRepositoryEndpoint struct {
	baseURL string
	apiKey  string
}

func NewDebtRegisterRepository(baseURL string, apiKey string) *DebtRegisterRepositoryEndpoint {
	return &DebtRegisterRepositoryEndpoint{
		baseURL: baseURL,
		apiKey:  apiKey,
	}
}

type bsnRequestModel struct {
	BSN string `json:"bsn"`
}

type schuldResponseModel struct {
	TypeSchuld                            string `json:"typeSchuld"`
	Omschrijving                          string `json:"omschrijving"`
	OpenstaandBedrag                      int    `json:"openstaandBedrag"`
	DatumOpenstaandBedragLaatstBijgewerkt string `json:"datumOpenstaandBedragLaatstBijgewerkt"`
	VorderingOvergedragen                 bool   `json:"vorderingOvergedragen"`
}

func (s *DebtRegisterRepositoryEndpoint) GetSchulden(bsn appdebtprocess.BSN) (*[]appdebtprocess.Schuld, error) {
	requestBody := bsnRequestModel{BSN: bsn.ToString()}
	requestBodyAsJson, err := json.Marshal(requestBody)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall request body: %v", err)
	}

	url := fmt.Sprintf("%s/api/v1/schulden", s.baseURL)
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return nil, fmt.Errorf("failed to create get debt request: %v", err)
	}

	req.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch debt: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status code while fetching debt: %d", resp.StatusCode)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	log.Printf("resp.Body: %s", string(body))

	var schuldenResponse []schuldResponseModel
	err = json.Unmarshal(body, &schuldenResponse)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return mapGetSchuldenResponseToModel(schuldenResponse), nil
}

func mapGetSchuldenResponseToModel(response []schuldResponseModel) *[]appdebtprocess.Schuld {
	result := []appdebtprocess.Schuld{}
	for _, v := range response {
		saldoDatum, err := time.Parse(time.RFC3339, v.DatumOpenstaandBedragLaatstBijgewerkt)
		if err != nil {
			log.Printf("error parsing saldo datum: %s", v.DatumOpenstaandBedragLaatstBijgewerkt)
		}
		schuld := appdebtprocess.NewSchuld(v.TypeSchuld, v.OpenstaandBedrag, saldoDatum, v.VorderingOvergedragen)
		result = append(result, *schuld)
	}

	return &result
}

func (s *DebtRegisterRepositoryEndpoint) GetHealthCheck() healthcheck.Result {
	name := "debt-register"
	url := fmt.Sprintf("%s/health/check", s.baseURL)
	timeout := 10 * time.Second
	start := time.Now()

	client := http.Client{
		Timeout: timeout,
	}

	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Printf("failed to create health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: nil,
		}
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	response, err := client.Do(request)
	if err != nil {
		log.Printf("failed to get health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: nil,
		}
	}

	body, err := ioutil.ReadAll(response.Body)

	if response.StatusCode != http.StatusOK && response.StatusCode != http.StatusServiceUnavailable {
		if err != nil {
			log.Printf("failed to parse body health check request: %s", err.Error())
		} else {
			log.Printf("failed to parse body health check request: %d - %s", response.StatusCode, string(body))
		}

		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: nil,
		}
	}
	log.Printf("resp.Body: %s", string(body))

	var healthCheckResponse healthcheck.Result

	err = json.Unmarshal(body, &healthCheckResponse)
	if err != nil {
		log.Printf("Unmarshal error: %v", err)
		healthCheckResponse.Status = healthcheck.StatusError
	}

	return healthCheckResponse
}
