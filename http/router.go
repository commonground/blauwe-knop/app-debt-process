// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"context"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"

	"appdebtprocess"
)

func NewRouter(schuldenUseCase *appdebtprocess.SchuldenUseCase) *chi.Mux {
	r := chi.NewRouter()

	r.Use(middleware.Logger)

	r.Route("/api/v1/", func(r chi.Router) {
		addOverzichtRoutes(r, schuldenUseCase)
	})

	return r
}
func addOverzichtRoutes(router chi.Router, schuldenUseCase *appdebtprocess.SchuldenUseCase) {
	router.Get("/schuldenoverzicht",
		func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), schuldenUseCaseKey, schuldenUseCase)
			handlerOverzicht(w, r.WithContext(ctx))
		})

	healthCheckHandler := healthcheck.NewHandler("app-debt-process", schuldenUseCase.GetHealthChecks())
	router.Route("/health", func(r chi.Router) {
		r.Get("/", healthCheckHandler.HandleHealth)
		r.Get("/check", healthCheckHandler.HandlerHealthCheck)
	})
}

type key int

const (
	schuldenUseCaseKey key = iota
)
