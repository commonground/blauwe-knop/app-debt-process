// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"time"

	"appdebtprocess"
)

type schuld struct {
	TypeSchuld            string `json:"type"`
	Saldo                 int    `json:"saldo"`
	SaldoDatum            string `json:"saldoDatum"`
	VorderingOvergedragen bool   `json:"vorderingOvergedragen"`
}

type overzichtViewModel struct {
	Organisatie             string   `json:"organisatie"`
	Schulden                []schuld `json:"schulden"`
	TotaalSaldo             int      `json:"totaalSaldo"`
	SaldoDatumLaatsteSchuld *string  `json:"saldoDatumLaatsteSchuld"`
}

func generateOverzichtViewModel(overzicht appdebtprocess.SchuldenOverzicht) overzichtViewModel {
	saldoDatumLaatsteSchuld := overzicht.SaldoDatumLaatsteSchuld()
	var saldoDatumLaatsteSchuldString *string
	if saldoDatumLaatsteSchuld != nil {
		formattedSaldo := saldoDatumLaatsteSchuld.Format(time.RFC3339)
		saldoDatumLaatsteSchuldString = &formattedSaldo
	}
	viewModel := overzichtViewModel{
		Organisatie:             overzicht.Organisatie(),
		TotaalSaldo:             overzicht.TotaalSaldo(),
		SaldoDatumLaatsteSchuld: saldoDatumLaatsteSchuldString,
	}

	for _, v := range overzicht.Schulden() {
		viewModel.Schulden = append(viewModel.Schulden, schuld{
			TypeSchuld:            v.TypeSchuld(),
			Saldo:                 v.Saldo(),
			SaldoDatum:            v.SaldoDatum().Format(time.RFC3339),
			VorderingOvergedragen: v.VorderingOvergedragen(),
		})
	}

	return viewModel
}
