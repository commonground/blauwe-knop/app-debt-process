// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"appdebtprocess"

	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"
)

type LinkRegisterRepositoryEndpoint struct {
	baseURL string
	apiKey  string
}

func NewLinkRegisterRepository(baseURL string, apiKey string) *LinkRegisterRepositoryEndpoint {
	return &LinkRegisterRepositoryEndpoint{
		baseURL: baseURL,
		apiKey:  apiKey,
	}
}

type bsnResponseModel struct {
	BSN string `json:"bsn"`
}

func (s *LinkRegisterRepositoryEndpoint) GetBSNFromLinkToken(linkToken string) (*appdebtprocess.BSN, error) {
	url := fmt.Sprintf("%s/link-tokens/%s", s.baseURL, linkToken)
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create get link token request: %v", err)
	}

	req.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch link token: %v", err)
	}

	if resp.StatusCode == http.StatusNotFound {
		return nil, nil
	} else if resp.StatusCode != http.StatusOK {
		log.Printf("linktoken: %s", linkToken)
		return nil, fmt.Errorf("unexpected status code while fetching link token: %d", resp.StatusCode)
	}

	bsnResponse := &bsnResponseModel{}
	err = json.NewDecoder(resp.Body).Decode(bsnResponse)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return appdebtprocess.NewBSN(bsnResponse.BSN)
}

func (s *LinkRegisterRepositoryEndpoint) GetHealthCheck() healthcheck.Result {
	name := "link-register"
	url := fmt.Sprintf("%s/health/check", s.baseURL)
	timeout := 10 * time.Second
	start := time.Now()

	client := http.Client{
		Timeout: timeout,
	}

	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Printf("failed to create health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: nil,
		}
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	response, err := client.Do(request)
	if err != nil {
		log.Printf("failed to get health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: nil,
		}
	}

	body, err := ioutil.ReadAll(response.Body)

	if response.StatusCode != http.StatusOK && response.StatusCode != http.StatusServiceUnavailable {
		if err != nil {
			log.Printf("failed to parse body health check request: %s", err.Error())
		} else {
			log.Printf("failed to parse body health check request: %d - %s", response.StatusCode, string(body))
		}

		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: nil,
		}
	}
	log.Printf("resp.Body: %s", string(body))

	var healthCheckResponse healthcheck.Result

	err = json.Unmarshal(body, &healthCheckResponse)
	if err != nil {
		log.Printf("Unmarshal error: %v", err)
		healthCheckResponse.Status = healthcheck.StatusError
	}

	return healthCheckResponse
}
