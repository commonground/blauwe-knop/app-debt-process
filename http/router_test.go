// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http_test

import (
	"errors"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"appdebtprocess"
	http_infra "appdebtprocess/http"
	"appdebtprocess/mock"
)

const MockLinktoken = "dummy-link-token"

func Test_CreateRouter_appdebtprocessoverzicht(t *testing.T) {
	type fields struct {
		debtRegisterRepository appdebtprocess.DebtRegisterRepositoryEndpoint
		linkRegisterRepository appdebtprocess.LinkRegisterRepository
	}
	type args struct {
		linkToken string
	}
	tests := []struct {
		name                   string
		fields                 fields
		args                   args
		expectedHTTPStatusCode int
		expectedBody           string
	}{
		{
			"without linkToken",
			fields{
				func() appdebtprocess.DebtRegisterRepositoryEndpoint {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockDebtRegisterRepositoryEndpoint(ctrl)

					return repo
				}(),
				func() appdebtprocess.LinkRegisterRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockLinkRegisterRepository(ctrl)

					return repo
				}(),
			},
			args{
				"",
			},
			http.StatusBadRequest,
			"link token required\n",
		},
		{
			"with invalid linkToken",
			fields{
				func() appdebtprocess.DebtRegisterRepositoryEndpoint {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockDebtRegisterRepositoryEndpoint(ctrl)

					return repo
				}(),
				func() appdebtprocess.LinkRegisterRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockLinkRegisterRepository(ctrl)
					repo.EXPECT().GetBSNFromLinkToken(MockLinktoken).Return(nil, errors.New("does not exist")).AnyTimes()

					return repo
				}(),
			},
			args{
				MockLinktoken,
			},
			http.StatusUnauthorized,
			"invalid link token\n",
		},
		{
			"find by BSN fails",
			fields{
				func() appdebtprocess.DebtRegisterRepositoryEndpoint {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					bsnModel, _ := appdebtprocess.NewBSN("814859094")

					schuld := appdebtprocess.NewSchuld(
						"Onroerende zaakbelasting",
						100,
						time.Date(2019, 06, 05, 0, 0, 0, 0, time.UTC),
						false,
					)

					repo := mock.NewMockDebtRegisterRepositoryEndpoint(ctrl)
					repo.EXPECT().GetSchulden(*bsnModel).Return(&[]appdebtprocess.Schuld{*schuld}, errors.New("error")).AnyTimes()

					return repo
				}(),
				func() appdebtprocess.LinkRegisterRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					bsnModel, _ := appdebtprocess.NewBSN("814859094")

					repo := mock.NewMockLinkRegisterRepository(ctrl)
					repo.EXPECT().GetBSNFromLinkToken(MockLinktoken).Return(bsnModel, nil).AnyTimes()

					return repo
				}(),
			},
			args{
				MockLinktoken,
			},
			http.StatusInternalServerError,
			"unable to get appdebtprocessoverzicht\n",
		},
		{
			"with valid link token",
			fields{
				func() appdebtprocess.DebtRegisterRepositoryEndpoint {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					bsnModel, _ := appdebtprocess.NewBSN("814859094")

					schuld := appdebtprocess.NewSchuld(
						"Onroerende zaakbelasting",
						100,
						time.Date(2019, 06, 05, 0, 0, 0, 0, time.UTC),
						false,
					)

					repo := mock.NewMockDebtRegisterRepositoryEndpoint(ctrl)
					repo.EXPECT().GetSchulden(*bsnModel).Return(&[]appdebtprocess.Schuld{*schuld}, nil).AnyTimes()

					return repo
				}(),
				func() appdebtprocess.LinkRegisterRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					bsnModel, _ := appdebtprocess.NewBSN("814859094")

					repo := mock.NewMockLinkRegisterRepository(ctrl)
					repo.EXPECT().GetBSNFromLinkToken(MockLinktoken).Return(bsnModel, nil).AnyTimes()

					return repo
				}(),
			},
			args{
				MockLinktoken,
			},
			http.StatusOK,
			"{\"organisatie\":\"my-organization\",\"schulden\":[{\"type\":\"Onroerende zaakbelasting\",\"saldo\":100,\"saldoDatum\":\"2019-06-05T00:00:00Z\",\"vorderingOvergedragen\":false}],\"totaalSaldo\":100,\"saldoDatumLaatsteSchuld\":\"2019-06-05T00:00:00Z\"}\n",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			appdebtprocessUseCase := appdebtprocess.NewSchuldenUseCase(test.fields.debtRegisterRepository, test.fields.linkRegisterRepository, "my-organization")
			router := http_infra.NewRouter(appdebtprocessUseCase)
			w := httptest.NewRecorder()

			request := httptest.NewRequest("GET", "/api/v1/schuldenoverzicht", nil)
			request.Header.Add("Authorization", test.args.linkToken)

			router.ServeHTTP(w, request)

			resp := w.Result()
			body, _ := ioutil.ReadAll(resp.Body)

			assert.Equal(t, test.expectedHTTPStatusCode, resp.StatusCode)
			assert.Equal(t, test.expectedBody, string(body))
		})
	}
}
