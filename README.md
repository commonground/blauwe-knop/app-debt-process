# App debt process

App debt process written in golang.

## Installation

Prerequisites:

- [Git](https://git-scm.com/)
- [Golang](https://golang.org/doc/install)
- [Modd](https://github.com/cortesi/modd)

1. Download the required Go dependencies:

```sh
go mod download
```

1. Start the app debt process:

```sh
go run cmd/app-debt-process/main.go --organization demo-org
```

Or run the app debt process using modd, which wil restart the app debt process on file changes.

```sh
modd
```

By default, the app debt process will run on port `8080`.

## Adding mocks

We use [GoMock](https://github.com/golang/mock) to generate mocks.
When you make updates to code for which there are mocks, you should regenerate the mocks.

**Regenerating mocks**

```sh
sh regenerate-gomock-files.sh
```

## Deployment

Prerequisites:

- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [helm](https://helm.sh/docs/intro/)
- deployment access to the haven cluster `dv-prod-common`

```sh
helm upgrade --install app-debt-process ./charts/app-debt-process -n bk-test
```
