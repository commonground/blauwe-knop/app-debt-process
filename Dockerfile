# Use go 1.x based on alpine image.
FROM golang:1.19.4-alpine AS build

ADD . /go/src/app-debt-process/
ENV GO111MODULE on
WORKDIR /go/src/app-debt-process
RUN go mod download
RUN go build -o dist/bin/app-debt-process ./cmd/app-debt-process

# Release binary on latest alpine image.
FROM alpine:latest

COPY --from=build /go/src/app-debt-process/dist/bin/app-debt-process /usr/local/bin/app-debt-process

# Add non-priveleged user
RUN adduser -D -u 1001 appuser
USER appuser
CMD ["/usr/local/bin/app-debt-process"]
